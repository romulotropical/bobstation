/obj/machinery/door/airlock
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_public.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/public
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_glass.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_glass_overlays.dmi'

/obj/machinery/door/airlock/external
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_external.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_external_overlays.dmi'

/obj/machinery/door/airlock/centcom
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_cc.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/grunge
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_cc_hatch.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_overlays.dmi'

/obj/machinery/door/airlock/vault
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_vault.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_vault_overlays.dmi'

/obj/machinery/door/airlock/hatch
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_hatch.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_overlays.dmi'

/obj/machinery/door/airlock/maintenance_hatch
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_hatch.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_overlays.dmi'

/obj/machinery/door/airlock/highsecurity
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_hightechsecurity.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_hightechsecurity_overlays.dmi'

/obj/machinery/door/airlock/shuttle
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_silver.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/glass_large
	icon = 'modular_skyrat/icons/eris/obj/doors/door_2x1metal.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/door_2x1_overlays.dmi'

/obj/machinery/door/airlock/command
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_command.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/security
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_security.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/engineering
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_engineering.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/medical
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_medical.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/maintenance
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_maint_extra.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/maintenance/external
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_external.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_external_overlays.dmi'

/obj/machinery/door/airlock/mining
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_mining.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/atmos
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_atmospherics.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/research
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_research.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/research
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_science.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/virology
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_medical_virology.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/freezer
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_freezer.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'

/obj/machinery/door/airlock/gold
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#ffff73"

/obj/machinery/door/airlock/silver
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#a1c8db"

/obj/machinery/door/airlock/diamond
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#3cf9ff"

/obj/machinery/door/airlock/uranium
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#2fb314"

/obj/machinery/door/airlock/plasma
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#d341ff"

/obj/machinery/door/airlock/bananium
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#fffb00"

/obj/machinery/door/airlock/sandstone
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#fff7b3"

/obj/machinery/door/airlock/titanium
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#c7c7c7"

/obj/machinery/door/airlock/wood
	icon = 'modular_skyrat/icons/eris/obj/doors/station/door_material.dmi'
	overlays_file = 'modular_skyrat/icons/eris/obj/doors/station/door_overlays.dmi'
	color = "#815400"
