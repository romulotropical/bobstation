/obj/item/coronerinfuser
	name = "knowledge infuser"
	icon = 'modular_skyrat/icons/obj/device.dmi'
	icon_state = "coronerinfuser"
	desc = "An experimental tool designed for transmitting knowledge from one person to another."
	flags_1 = CONDUCT_1
	item_flags = NOBLUDGEON
	slot_flags = ITEM_SLOT_BELT
	throwforce = 3
	w_class = WEIGHT_CLASS_TINY
	throw_speed = 3
	throw_range = 7
	custom_materials = list(/datum/material/iron=200)
	var/hasbuffer = FALSE
	var/currentbuffer = 1
	var/storedskill = 0



/obj/item/coronerinfuser/attack_self(mob/user)
	if (!hasbuffer)
		var/list/scam_list = list("Melee combat" = 1, "Ranged combat" = 2, "Medical" = 3, "Surgery" = 4, "Chemistry" = 5, "Construction" = 6, "Electronics" = 7, "Research" = 8, "Cooking" = 9, "Agriculture" = 10)
		var/scam = input(user, "What topic would you like to extract?", "[capitalize(src.name)]") as anything in scam_list
		currentbuffer = scam_list[scam]
		to_chat(user, "<span class='info'>You set the infuser to extract information about the following topic: [capitalize(user.mind.mob_skills[user.mind.mob_skills[currentbuffer]].name)]</span>")
	else
		hasbuffer = FALSE
		storedskill = 0
		to_chat(user, "The infuser beeps as you clean its database")


/obj/item/coronerinfuser/attack(mob/living/M, mob/living/carbon/human/user)
	if (!hasbuffer && M.stat != DEAD)
		if (storedskill < M.mind.mob_skills[M.mind.mob_skills[currentbuffer]].level)
			var/msg = "<span class='info'>You copy over the knowledge to your buffer from [M].</span><br>"
			var/exskill = M.mind.mob_skills[M.mind.mob_skills[currentbuffer]].level
			storedskill = exskill
			exskill = exskill / 20 * 100
			msg += "<span class='info'><b>Now the infuser has [exskill]% of the [capitalize(user.mind.mob_skills[user.mind.mob_skills[currentbuffer]].name)] database complete!</b></span>"
			hasbuffer = TRUE
			to_chat(user, msg)
		else
			var/msg = "<span class='warning'>Extraction failed: Subject does not have any additional info not already catalogued to the current database.</span>"
			to_chat(user, msg)
	else if (!hasbuffer && M.stat == DEAD)
		to_chat(user, "<span class='warning'>Extraction failed: Subject is dead!</span>")
	else
		if (M.stat == DEAD)
			if (hasbuffer)
				if (storedskill > M.mind.mob_skills[M.mind.mob_skills[currentbuffer]].level)
					M.mind.mob_skills[M.mind.mob_skills[currentbuffer]].level = storedskill
					hasbuffer = FALSE
					storedskill = 0
					to_chat(user, "<span class='info'><b>The infuser beeps as the infusion procedure succeeds!</b></span>")
				else
					to_chat(user, "<span class='warning'>Infusion failed: Target already has a higher knowledge at this topic than the database can provide!")
			else
				to_chat(user, "<span class='warning'>The current knowledge buffer is empty!</span>")
		else
			to_chat(user, "<span class='warning'>Infusion failed: Target is not dead!</span>")

