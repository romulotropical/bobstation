/datum/job/hos
	title = "Chief Enforcer"

/datum/outfit/job/hos
	backpack_contents = list(/obj/item/pda/heads/hos=1,
							/obj/item/melee/mace=1,
							)
	belt = /obj/item/storage/belt/sabre/hos
	suit_store = /obj/item/gun/energy/e_gun/advtaser/large
	gloves = /obj/item/clothing/gloves/color/black/ce
